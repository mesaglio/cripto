##  Instructions

### Mac/Linux

- Download the repository

- Open the terminal, and move to the LinuxOrMac directory

- Then use this code:
 - to encrypt `./cripto 1 [FILE] `
 - to decrypt `./cripto 2 [FILE]`

---

### Windows

- Download the repository

- Open cmd, and move to the Windows directory

- Then use this code:
 - to encrypt `cripto.exe 1 [FILE] `
 - to decrypt `cripto.exe 2 [FILE]`

---