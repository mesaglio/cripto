#include <time.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#define bool int
#define true 1
#define false 0
#define inicioMayus 65
#define finMayus 90
#define inicioMinus 97
#define finMinus 122

bool esMinus(char c){
     return c >= 97 && c <= 122;
}
bool esMayus(char c){
	return c >= 65 && c <= 90;
}
int encript(char c,int tiempo){
     int aux = (char)c;
     if (aux == 32) {
          return aux;
     }
     aux += tiempo;
     return aux;
}
int desEncritp(char c,int tiempo){
     int aux = (char)c;
     if (aux == 32) {
          return aux;
     }
     aux -= tiempo;
     return aux;
}

int main(int argc, char const *argv[])
{
	char caracter1,caracter2;
	FILE* entrada;
	FILE* encriptado;
	FILE* desEncriptado;
	char* encriptar = "1";
	char* desEncriptar = "2";

	if (argc == 3)
	{
		if (strcmp(argv[1],encriptar) == 0)//encriptar
		{
               time_t t = time(NULL);
               struct tm tm = *localtime(&t);
               int aux = tm.tm_sec/5;

               entrada = fopen(argv[2],"r");
			encriptado = fopen("archivoEncriptado.txt","w");

               putc(aux,encriptado);

               caracter1 = fgetc(entrada);
		 	while(caracter1 != EOF){
                    int cripto = encript(caracter1,aux);
		 		putc(cripto,encriptado);
		 		caracter1 = fgetc(entrada);
			}
			fclose(encriptado);
			fclose(entrada);
			return 0;
		}
		if (strcmp(argv[1],desEncriptar) == 0)//desencriptar
		{
			encriptado = fopen(argv[2],"r");
               char times = fgetc(encriptado);
               int aux = (int) times;
			desEncriptado = fopen("archivoOriginal.txt","w");
			caracter2 = fgetc(encriptado);
			while(caracter2 != EOF){
		 		int cripto = desEncritp(caracter2, aux);
		 		putc(cripto,desEncriptado);
		 		caracter2 = fgetc(encriptado);
		 	}

		 	fclose(encriptado);
		 	fclose(desEncriptado);

		 	return 0;
		}else {
			printf("Error en el tipo de operacion\n");
			printf("Para cifrar ./program 1 ARCHIVO\n");
			printf("Para descifrar ./program 2 ARCHIVO\n");
			return -1;
		}
	}
	else{
		printf("Error en cantidad de entrada\n");
		printf("Ingrese ./program tipoDeOperacion ARCHIVO\n");
		return -2;
	}
	return 0;
}
